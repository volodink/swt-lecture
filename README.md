# Защищенные веб-технологии

Введение - [ [Презентация](https://volodink.gitlab.io/swt-lecture/swt_intro.pdf) ]

OSINT. Часть 1 - [ [Презентация](https://volodink.gitlab.io/swt-lecture/osint_part1.pdf) ]

OSINT. Часть 2 - [ [Презентация](https://volodink.gitlab.io/swt-lecture/osint_part2.pdf) ]

OSINT. Часть 3 - [ [Презентация](https://volodink.gitlab.io/swt-lecture/osint_part3.pdf) ]

OSINT. Часть 4 - [ [Презентация](https://volodink.gitlab.io/swt-lecture/osint_part4.pdf) ]

External network - [ [Презентация](https://volodink.gitlab.io/swt-lecture/external.pdf) ]
