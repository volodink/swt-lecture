all: clean prepare
	cd lessons/intro && make && cp dist/intro.pdf ../../dist/swt_intro.pdf
	cd lessons/osint.part.1 && make && cp dist/osint_part1.pdf ../../dist/osint_part1.pdf
	cd lessons/osint.part.2 && make && cp dist/osint_part2.pdf ../../dist/osint_part2.pdf
	cd lessons/osint.part.3 && make && cp dist/osint_part3.pdf ../../dist/osint_part3.pdf
	cd lessons/osint.part.4 && make && cp dist/osint_part4.pdf ../../dist/osint_part4.pdf
	cd lessons/external && make && cp dist/external.pdf ../../dist/external.pdf

clean:
	rm -rf dist

prepare:
	mkdir -p dist

gitpod-bootstrap:
	sudo apt-get update && sudo apt-get dist-upgrade -y
