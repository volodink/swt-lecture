---
marp: true
theme: lection
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#


# Защищенные веб-системы

#

## OSINT. Часть 1.

#

#

#

#

#

#

##### Константин Володин


---

# OSINT (Open Source Intelligence)

_-_ это процесс сбора, анализа и использования общедоступной информации из различных открытых источников для получения разведывательных данных и аналитики. 

Может включать в себя информацию из интернета, социальных сетей, новостных источников, государственных документов и многого другого.


---

![bg](https://media.lpgenerator.ru/uploads/2023/06/01/10.jpg)

---

# Цели OSINT 

1. Понимание обстановки

2. Разведка

3. Аналитика

4. Безопасность

---

<!-- _paginate: false -->

![bg](https://www.collidu.com/media/catalog/product/img/0/b/0bbb8066ef411efc86d0d8ff9ff3921dff9b2c1907f835e4d9db8716fab27a9d/open-source-intelligence-osint-slide1.png)


---

![bg h:95%](img/1_NzY766OoM7LJHqFVzdXmKw.webp)

---

# Аспекты OSINT

1. Идентификация уязвимостей
2. Социальная инженерия
3. Исследование открытых данных

---

# OSINT вокруг нас

1. Анализ финансовых данных и рынков
2. Поддержка в поиске работников и рекрутинге
3. Обнаружение фейковых новостей и дезинформации
4. Мониторинг кризисов и чрезвычайных ситуаций

---

# Are you a Bad guy?


OSINT может использоваться не только в законных, но и нелегальных целях. Например, с помощью разведки в открытых ресурсах злоумышленник может:

- украсть персональные данные пользователей или конфиденциальную информацию о деятельности человека/организации;
- получить компромат и использовать его для шантажа, вымогательства, нанесения ущерба репутации и т.д.;
- нарушить работу информационных, производственных или других ресурсов организации.

---

# Законодательный аспект OSINT

1. Защита персональных данных
2. Авторские права
3. Законодательство о кибербезопасности
4. Законодательство о приватности и регулирование интернета
5. Этические нормы

---
<!-- _class: lead -->

https://osintframework.com/

---

# Задача

Получить список доступных e-mail адресов

# Тулы

1. theHarvester - https://github.com/laramies/theHarvester
2. recon-ng - [https://github.com/lanmaster53/recon-ng](https://github.com/lanmaster53/recon-ng)
3. Shodan
4. Прямое скачивание и анализ
- win vs linux
- readymade vs DIY

---

# Аспекты

_-_ Парсинг почты из html
_-_ Парсинг почты из pdf

![bg right:50%](https://www.meme-arsenal.com/memes/1767010b00c56ee05a0afc3339b75449.jpg)

---

# Что делать с почтой

[haveibeenpwned.com](haveibeenpwned.com) — проверка на наличие Email’а в слитых базах
[Ghunt](https://github.com/mxrch/GHunt) — поиск информации о владельце Google аккаунта



---



---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
