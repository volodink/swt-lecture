#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <string.h>
#include <vector>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define PORT 9000
#define BUFFER_SIZE 1024

// Simple function to replace all occurrences of a substring with another
std::string replaceAll(std::string str, const std::string &from, const std::string &to)
{
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // In case "to" contains "from"
    }
    return str;
}

// Function to parse the HTTP request and extract the method and path
std::pair<std::string, std::string> parseRequest(const std::string &request)
{
    std::istringstream iss(request);
    std::string method, path;
    getline(iss, method, ' ');
    getline(iss, path);
    path = replaceAll(path, "%20", " "); // Replace URL-encoded spaces
    return {method, path};
}

// Function to handle the HTTP GET request
void handleGet(int clientSocket, const std::string &path)
{
    // Simulate serving a basic HTML response
    std::string response =
        "HTTP/1.1 200 OK\r\n"
        "Content-Type: text/html\r\n\r\n"
        "<!DOCTYPE html><html><body><h1>Hello from the server!</h1></body></html>";
    send(clientSocket, response.c_str(), response.length(), 0);
}

// Function to handle the HTTP POST request (simple echo back)
void handlePost(int clientSocket, const std::string &path)
{
    // Read the POST data
    char buffer[BUFFER_SIZE];
    int bytesRead;
    std::string postData;
    while ((bytesRead = recv(clientSocket, buffer, BUFFER_SIZE - 1, 0)) > 0)
    {
        buffer[bytesRead] = '\0';
        postData += buffer;
    }

    // Simulate echoing back the received data
    std::string response =
        "HTTP/1.1 200 OK\r\n"
        "Content-Type: text/plain\r\n\r\n" +
        postData;
    send(clientSocket, response.c_str(), response.length(), 0);
}

int main()
{
    // Create a server socket
    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1)
    {
        std::cerr << "Error creating socket" << std::endl;
        return 1;
    }

    // Bind the socket to an address
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(PORT);
    if (bind(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        std::cerr << "Error binding socket" << std::endl;
        close(serverSocket);
        return 1;
    }

    // Listen for incoming connections
    if (listen(serverSocket, 5) == -1)
    {
        std::cerr << "Error listening for connections" << std::endl;
        close(serverSocket);
        return 1;
    }

    std::cout << "Server listening on port " << PORT << std::endl;

    while (true)
    {
        // Accept a connection
        int clientSocket = accept(serverSocket, nullptr, nullptr);
        if (clientSocket == -1)
        {
            std::cerr << "Error accepting connection" << std::endl;
            continue;
        }

        std::cout << "Connection accepted!" << std::endl;

        // Receive the request from the client
        char buffer[BUFFER_SIZE];
        std::string request;
        int bytesRead;
        while ((bytesRead = recv(clientSocket, buffer, BUFFER_SIZE - 1, 0)) > 0)
        {
            buffer[bytesRead] = '\0';
            request += buffer; // Accumulate received data

            // Check if the entire request has been received
            if (strstr(request.c_str(), "\r\n\r\n") != nullptr)
            { // Look for double carriage return and newline
                break;
            }
        }

        std::cout << request << std::endl;

        // Parse the request to extract method and path
        std::pair<std::string, std::string> requestInfo = parseRequest(request);
        std::string method = requestInfo.first;
        std::string path = requestInfo.second;

        // Handle the request based on the method
        if (method == "GET")
        {
            handleGet(clientSocket, path);
        }
        else if (method == "POST")
        {
            handlePost(clientSocket, path);
        }
        else
        {
            // Send a 405 Method Not Allowed response for other methods
            std::string response =
                "HTTP/1.1 405 Method Not Allowed\r\n"
                "Allow: GET, POST\r\n\r\n";
            send(clientSocket, response.c_str(), response.length(), 0);
        }

        // Close the client socket
        close(clientSocket);
    }

    // Close the server socket
    close(serverSocket);

    return 0;
}
