---
marp: true
theme: lection
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#


# Защищенные веб-системы

#

## OSINT. Часть 3.

## Активный сбор информации

#

#

#

#

#

##### Константин Володин


---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Публичный дисклеймер


---

<!-- _paginate: false -->

# Публичная оферта

1. Информация несет __ОБРАЗОВАТЕЛЬНЫЙ__ характер, чтобы уберечь пользователей от утечки информации и чувствительных данных, а также повысить бдительность в защите своих данных.

2. Авторы не поощряют действия хакеров, которые действуют вне законов государства, также автор сам таким не является.

3. Я __%userName%__, проходящий данный курс, буду проводить исследование безопасности после одобрения перечня мероприятий в учебное время и непротиворечащее __УК РФ__ во внеучебное, а также буду __ЧИТАТЬ МАНЫ__ по ключам тулов, затрагивающие RPS, количество потоков, глубину исседования и т.п, способное на время остановить или препятствовать существенно работе исследуемой инфраструктуре.  ФИО. Подпись.


---

# План лекции

1. Активный сбор информации 
2. Что дает информация активного сбора
3. Версии ПО сервисов


---

# Активный сбор информации 

Активный сбор информации в OSINT - это метод сбора информации, который задействует взаимодействие с источником информации.


---

# Методы активного сбора информации

_-_ __Социальная инженерия:__ использование психологических методов для получения информации от людей.
_-_ __Фишинг:__ рассылка электронных писем или SMS-сообщений, которые выглядят как легитимные, чтобы побудить людей раскрыть конфиденциальную информацию.
_-_ __Взлом:__ использование технических методов для получения доступа к компьютерным системам и сетям.
_-_ __Скрытое наблюдение:__ использование камер, микрофонов или других устройств для сбора информации о людях или объектах.


---

# Преимущества активного сбора информации

Может быть более точной и достоверной, чем информация из пассивных источников.
Может быть использован для получения информации, которая недоступна из пассивных источников.

# Недостатки активного сбора информации

Может быть незаконным или неэтичным.
Может быть опасным.
Может быть трудоемким.


---

# Инструменты активного сбора информации

Инструменты социальной инженерии: 
- __Metasploit__
- __Social-Engineer Toolkit__

Инструменты взлома: 
- __nmap__ 
- __Metasploit__
- __John the Ripper__


---

# Metasploit

Metasploit - это фреймворк с открытым исходным кодом, который используется для проникновения, тестирования на проникновение и разработки эксплойтов.

[https://www.metasploit.com/](https://www.metasploit.com/)


---

# Возможности Metasploit


__Широкий выбор эксплойтов:__ Metasploit содержит эксплойты для различных уязвимостей, включая уязвимости операционных систем, веб-приложений, сетевых устройств и т.д.

__Мощные инструменты для тестирования на проникновение:__ Metasploit includes a variety of tools for penetration testing, including a port scanner, a network sniffer, and a web application scanner.

__Возможность разработки эксплойтов:__ Metasploit allows you to develop your own exploits.

__Создание пост-эксплуатационных модулей:__ Metasploit allows you 
to create modules that can be used to take control of a compromised system.


---

# Использование Metasploit

Metasploit доступен в двух вариантах: Metasploit Framework и Metasploit Pro.

Metasploit Framework - это бесплатная версия Metasploit.

Metasploit Pro - это платная версия Metasploit


---

# Nmap: Сканер сети с открытым исходным кодом

__Nmap ("Network Mapper")__ - это бесплатная  и открытая  утилита для обнаружения сетей  и аудита безопасности.

Nmap is used to discover hosts and services on a computer network by sending packets and analyzing the responses.

__Ресурсы__
[https://nmap.org/](https://nmap.org/)
[https://en.wikipedia.org/wiki/Nmap](https://en.wikipedia.org/wiki/Nmap)
[freecodecamp.org // What is Nmap and How to Use it – A Tutorial for the Greatest Scanning Tool of All Time](https://www.freecodecamp.org/news/what-is-nmap-and-how-to-use-it-a-tutorial-for-the-greatest-scanning-tool-of-all-time/)


---

<!-- _class: manylines -->

# Nmap features

__Fast scan (_nmap -F [target]_)__ – Performing a basic port scan for fast result.

__Host discovery__ – Identifying hosts on a network.
For example, listing the hosts that respond to TCP and/or ICMP requests or have a particular port open.

__Port scanning__ – Enumerating the open ports on target hosts.

__Version detection__ – Interrogating network services on remote devices 
to determine __application name and version number__.

__Ping Scan__ – Check host by sending ping requests.

__TCP/IP stack fingerprinting__ – Determining the __operating system and hardware characteristics__ of network devices based on observations of network activity of said devices.

__Scriptable interaction with the target__ – using Nmap Scripting Engine (NSE) and Lua programming language.


---

<!-- _class: manylines -->

# Typical uses of Nmap

__Auditing the security of a device or firewall__ by identifying the network connections which can be made to, or through it.

__Identifying open ports__ on a target host in preparation for auditing.

__Network inventory, network mapping, maintenance and asset management__.

__Auditing the security of a network__ by identifying new servers.

__Generating traffic to hosts on a network__, response analysis and response time measurement.

__Finding and exploiting vulnerabilities__ in a network.

__DNS queries and subdomain search__.

---

<!-- _class: manylines -->

# Berkeley sockets

Berkeley sockets is an application programming interface (API) for Internet sockets and Unix domain sockets, used for inter-process communication (IPC). It is commonly implemented as a library of linkable modules. It originated with the 4.2BSD Unix operating system, which was released in 1983.

A socket is an abstract representation (handle) for the local endpoint of a network communication path. The Berkeley sockets API represents it as a file descriptor (file handle) in the Unix philosophy that provides a common interface for input and output to streams of data.

Berkeley sockets evolved with little modification from a de facto standard into a component of the POSIX specification. The term POSIX sockets is essentially synonymous with Berkeley sockets, but they are also known as BSD sockets, acknowledging the first implementation in the Berkeley Software Distribution.


---
<!-- _class: manylines -->
## Заголовочные файлы для работы с сокетами

__<sys/socket.h>__
Базовые функции сокетов BSD и структуры данных.

__<netinet/in.h>__
Семейства адресов/протоколов PF_INET и PF_INET6. Широко используются в сети Интернет, включают в себя IP-адреса, а также номера портов TCP и UDP.

__<sys/un.h>__
Семейство адресов PF_UNIX/PF_LOCAL. Используется для локального взаимодействия между программами, запущенными на одном компьютере. В компьютерных сетях не применяется.

__<arpa/inet.h>__
Функции для работы с числовыми IP-адресами.

__<netdb.h>__
Функции для преобразования протокольных имен и имен хостов в числовые адреса. Используются локальные данные аналогично DNS.

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Server Stages


---

# 1. Creating the Server Socket

We create socket by using the _socket()_ system call. It is defined inside the _<sys/socket.h>_ header file.

Syntax

```cpp
int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
```

- socketfd: It is the file descriptor for the socket.
- AF_INET: It specifies the IPv4 protocol family.
- SOCK_STREAM: It defines that the TCP type socket.

---

# 2. Defining Server Address

We then define the server address using the following set of statements

```cpp
sockaddr_in serverAddress;
serverAddress.sin_family = AF_INET;
serverAddress.sin_port = htons(8080);
serverAddress.sin_addr.s_addr = INADDR_ANY;
```

- sockaddr_in: It is the data type that is used to store the address of the socket.
- htons(): This function is used to convert the unsigned int from machine byte order to network byte order.
- INADDR_ANY: It is used when we don’t want to bind our socket to any particular IP and instead make it listen to all the available IPs.

---

# 3. Binding the Server Socket

Then we bind the socket using the bind() call as shown.

```cpp
bind(serverSocket, (struct sockaddr*)&serverAddress, sizeof(serverAddress));
```

---

# 4. Listening for Connections

We then tell the application to listen to the socket refffered by the serverSocket.

```cpp
listen(serverSocket, 5);
```

---

# 5. Accepting a Client Connection

The _accept()_ call is used to accept the connection request that is recieved on the socket the application was listening to.

```cpp
int clientSocket = accept(serverSocket, nullptr, nullptr);
```
---

# 6. Receiving Data from the Client

Then we start receiving the data from the client. We can specify the required buffer size so that it has enough space to receive the data sent the the client. The example of this is shown below.

```cpp
char buffer[1024] = {0};
recv(clientSocket, buffer, sizeof(buffer), 0);
cout << "Message from client: " << buffer << endl;
```

---

# 7. Closing the Server Socket

We close the socket using the close() call and the associated socket descriptor.

```cpp
close(serverSocket);
```

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Live demo

---

<!-- _class: big -->

## Задача

Получить максимально возможную информацию о ресурсах c помощью активного сканирования

## Тулы и методы
```bash
>> nmap (-T2)
```
- изучить возможности и флаги тула
- определение версий ПО сервисов, используемых на ресурсах

## Ресурсы
- https://fastiraz.gitbook.io/doc/documentations/hack/nmap
- https://kmb.cybber.ru/


---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
