---
marp: true
theme: lection
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#


# Защищенные веб-системы

#

## OSINT. Часть 2.

#

#

#

#

#

#

##### Константин Володин

---

# Тулы и источники информации

1. Команды(программы или скрипты) которые запускаются из командной строки
2. Файлы на сайтах и др. источниках, закешированные поисковиками файлы, которые должны быть недоступны или исключены из списков кроулеров

---

# Примечания по командам

1. Читайте описание и ключи для уменьшения проблем
2. Только некоторые комманды доступны в стандартных дистрибутивах Linux
3. Приведенные команды уже входят в состав ПО Kali Linux


---

# Полезные команды

- host
- whatweb

---

# host

```bash
>> host
```

Host is a simple utility for performing DNS lookups. It is normally used to convert names to IP addresses and vice versa. When no arguments or options are given, host prints a short summary of its command-line arguments and options.

Host — это простая утилита для поиска DNS. Обычно он используется для преобразования имен в IP-адреса и наоборот. Если аргументы или параметры не указаны, хост печатает краткую сводку аргументов и параметров командной строки.

---

<!-- _class: manylines -->

# whatweb

```
>> whatweb
```

WhatWeb идентифицирует веб-сайты. 

Его цель ответить на вопрос: «Что это за сайт?». 

WhatWeb распознает веб-технологии, включая системы управления контентом (CMS), платформы для блогов, пакеты статистики/аналитики, библиотеки JavaScript, веб-серверы и встроенные устройства. 

WhatWeb имеет более 1800 плагинов, каждый из которых распознает что-то свое. 

WhatWeb также идентифицирует номера версий, адреса электронной почты, идентификаторы учетных записей, модули веб-платформы, ошибки SQL и многое другое.

---

# Источники информации

- Dnsdumpster
- robots.txt
- XML sitemaps
- Google Dorks search system


---

<!-- _class: manylines -->

# Dnsdumpster web tools 

Is a FREE domain research tool that can discover hosts related to a domain. Finding visible hosts from the attackers perspective is an important part of the security assessment process.

Это БЕСПЛАТНЫЙ инструмент исследования домена, который может обнаруживать хосты, связанные с доменом. Поиск видимых хостов с точки зрения злоумышленников является важной частью процесса оценки безопасности.

[https://dnsdumpster.com/](https://dnsdumpster.com/)

---

<!-- _class: manylines -->

# robots.txt file inside website 

A robots.txt file tells search engine crawlers which URLs the crawler can access on your site. This is used mainly to avoid overloading your site with requests; it is not a mechanism for keeping a web page out of Google. To keep a web page out of Google, block indexing with noindex or password-protect the page.

Файл robots.txt сообщает поисковым роботам, к каким URL-адресам робот может получить доступ на вашем сайте. Это используется главным образом для того, чтобы избежать перегрузки вашего сайта запросами; это не механизм предотвращения попадания веб-страницы в Google. Чтобы защитить веб-страницу от Google, заблокируйте индексирование с помощью noindex или защитите страницу паролем.

[https://ok.ru/robots.txt](https://ok.ru/robots.txt)


---

<!-- _class: manylines -->

# XML sitemaps file inside website

An XML sitemap is a file that lists a website's essential pages, making sure Google can find and crawl them all. It also helps search engines understand your website structure. You want Google to crawl every important page of your website.

Карта сайта XML – это файл, в котором перечислены основные страницы веб-сайта, благодаря чему Google может найти и просканировать их все. Это также помогает поисковым системам понять структуру вашего сайта. Вы хотите, чтобы Google просканировал каждую важную страницу вашего сайта.

__Пример:__
Sitemap: https://ok.ru/sitemap-index-misc.xml.gz
Sitemap: https://ok.ru/sitemap-index-game.xml.gz
Sitemap: https://ok.ru/sitemap-index-gift.xml.gz
Sitemap: https://ok.ru/sitemap-index-content-search.xml.gz
...


---

# Google Dorks

The is an index of search queries used to find publicly available information, intended for pentesters and security researchers. 

Это индекс поисковых запросов, используемый для поиска общедоступной информации, предназначенный для пентестеров и исследователей безопасности.

[Google Hacking Database](https://www.exploit-db.com/google-hacking-database)


---

<!-- _class: manylines -->

# Задача

Получить максимально возможную информацию о ресурсах:
- penzgtu.ru
- edu.penzgtu.ru
- stinfo.penzgtu.ru

# Тулы и методы

- host
- whatweb
- Dnsdumpster
- robots.txt
- XML sitemaps
- Google Dorks


---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
