---
marp: true
theme: lection
paginate: true
---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#


# Защищенные веб-системы

#

## Внешняя сеть/Внешний периметр

#

#

#

#

#

##### Константин Володин


---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Публичный дисклеймер


---

<!-- _paginate: false -->

# Публичная оферта

1. Информация несет __ОБРАЗОВАТЕЛЬНЫЙ__ характер, чтобы уберечь пользователей от утечки информации и чувствительных данных, а также повысить бдительность в защите своих данных.

2. Авторы не поощряют действия хакеров, которые действуют вне законов государства, также автор сам таким не является.

3. Я __%userName%__, проходящий данный курс, буду проводить исследование безопасности после одобрения перечня мероприятий в учебное время и непротиворечащее __УК РФ__ во внеучебное, а также буду __ЧИТАТЬ МАНЫ__ по ключам тулов, затрагивающие RPS, количество потоков, глубину исседования и т.п, способное на время остановить или препятствовать существенно работе исследуемой инфраструктуре.  ФИО. Подпись.


---

# План лекции

1. Что такое внешняя/внутренняя сети?
2. Виды сетей
3. DMZ
4. Хостинг
5. CMS: wp, bitrix, moodle, jumla
6. Хранение паролей, хеши но это не безопасно
7. Точка входа из внешки во внутрянку
8. Учетная запись пользователя
9. Реверс шелл

---

# DMZ

![bg h:95%](https://www.itpedia.nl/wp-content/uploads/2023/01/DMZ.png)


---

<!-- _class:  -->

# Контейнерный хостинг
![w:650px](https://docs.aws.amazon.com/images/AmazonECS/latest/bestpracticesguide/images/networkmode-host.png)
https://docs.aws.amazon.com/AmazonECS/latest/bestpracticesguide/networking-networkmode-host.html

---

# Amazon ECS Best Practices

https://nathanpeck.com/amazon-ecs-networking-best-practices/files/ecs-networking-best-practices-guide.pdf

# Netflix

https://gotocon.com/dl/goto-amsterdam-2016/slides/RuslanMeshenberg_MicroservicesAtNetflixScaleFirstPrinciplesTradeoffsLessonsLearned.pdf

---

# CMS

![bg w:100%](https://t4.ftcdn.net/jpg/04/49/32/73/240_F_449327309_INPAUv5iWWvyAuG9ICkaOfHUbEUbAp2W.jpg)

---

# Хранение паролей, хеши

https://www.hivesystems.com/blog/are-your-passwords-in-the-green

---

# Reverse shell / Реверс шелл 

Реверс шелл, также известный как обратный шелл, удаленный шелл или connect-back shell, представляет собой метод, который злоумышленники используют для получения удаленного доступа к скомпрометированной системе.

---

# Принцип работы

__Злоумышленник:__

1. Запускает на своей машине сервер (слушатель), который будет ожидать подключения.
2. Создает полезную нагрузку (payload) - вредоносный код, который будет запущен на целевой системе.

---

# Жертва

1. Запускает полезную нагрузку на своей системе.

2. Полезная нагрузка:

- Подключается к серверу злоумышленника.
- Создает на целевой системе обратную оболочку (shell).
- Передает злоумышленнику управление обратной оболочкой.

---

# Злоумышленник


Использует обратную оболочку для выполнения команд на целевой системе.

Может:
- Скачивать и загружать файлы.
- Изменять системные настройки.
- Устанавливать вредоносное ПО.
- И многое другое))

Пояснение: __Зависит от уровня привигений__


---

<!-- _class: manylines -->

# Преимущества для злоумышленника


1. Обход брандмауэров:
- Обратная оболочка инициирует соединение с сервером злоумышленника.
- Это позволяет обойти брандмауэры, которые могут блокировать входящие соединения.
2. Скрытность:
- Обратная оболочка может быть запущена в фоновом режиме, не привлекая внимания пользователя.
3. Универсальность:
- Реверс шеллы могут быть реализованы на различных языках программирования и платформах.

---

<!-- _class: manylines -->

# Способы защиты

1. Антивирусное ПО:
- Может обнаружить и заблокировать полезную нагрузку.

2. Системы обнаружения вторжений (IDS):
- Могут обнаружить подозрительную активность, связанную с обратными шеллами.
2. Ограничение доступа к сети:
- Блокировка доступа к серверу злоумышленника.
3. Повышение осведомленности пользователей:
- Обучение пользователей распознавать и не запускать подозрительные файлы.

---

<!-- _class: manylines -->

# A typical reverse shell attack scenario

Since getting a reverse shell is just one stage of an attack, here is an example chain of events that involves the use of a reverse shell:

1. The attacker discovers a __remote code execution (RCE)__ vulnerability in _www.example.com_ and also establishes that _www.example.com_ lets users upload their own images without testing whether the uploaded file is a valid image.
2. The attacker uploads a Python reverse shell script to _www.example.com_, disguising it as an image by calling the file __test.jpg__.
The attacker uses the RCE vulnerability to execute the uploaded __test.jpg__ Python script.
3. The __test.jpg__ script establishes a connection to port __80__ of the attacker’s machine. The attacker is now able to send shell commands to the __www.example.com__ web server.
4. The attacker can now attempt __privilege escalation__. For example, they may find a vulnerability in the operating system that allows them to gain __root access to the server__.

https://www.invicti.com/learn/reverse-shell/


---

# Пользователи 

```bash
less /etc/passwd
```

Домашние папки: 

```bash
/home/user
/home/vasya
/home/volodink
```

Домашняя папка пользователя __root__

```bash
/root
```

![bg right:70% h:95%](img/passwd.jpg)

---

# SSH и SCP

[SSH cheat sheet list](https://quickref.me/ssh.html)

[Примеры работы с SCP](https://losst.pro/kopirovanie-fajlov-scp)


---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> live demo


---

# Тренировки

[standoff365.com](https://standoff365.com/)


---

<!-- _class: manylines -->

# Задача
1. Скачать образ виртуальной машины - https://drive.google.com/file/d/1nUR5IkftUdj47thyqdVGU98_DprwBvIi/view?usp=sharing
2. Настроить сеть (см. Notion)
3. Провести ряд мероприятий и получить доступ к внутренней сети под любым пользователем

## Подсказки

1. Воспользоваться *netdiscover* для поиска адреса целевой машины
2. *nmap* нужен и на этой неделе)
3. Внесите доменное имя в /etc/hosts
4. Установите расширение для браузера Wappalyzer и поглядите технологии веб-ресурса
5. Для каждой технологии есть свой сканнер уязвимостей)
6. Что насчет 22 порта?)

---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
